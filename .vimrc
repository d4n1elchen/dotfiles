" Set encoding
scriptencoding utf-8
set encoding=utf-8

" Appearance
syntax on
set nu
" set cursorline
set showmatch        " automatically show matching brackets.
set showmode         " show the current mode
set showcmd          " show the typing command
set ruler            " show the cursor position all the time
set laststatus=2     " make the last line where the status is two lines deep so you can see status always
" set background=dark  " Use colours that work well on a dark background (Console is usually black)
" set t_Co=256
""""

" Settings about tab and indent
" - Global
set expandtab
set tabstop=4
set shiftwidth=4
set autoindent
set smarttab
set cindent
filetype plugin indent on

" - Specific file type
autocmd FileType css,sass,scss setlocal sw=1 ts=1
autocmd FileType javascript,html,pug,yaml setlocal sw=2 softtabstop=2
""""

" Key mappings
" - jk to esc
inoremap jk <ESC>
" - space to save
nnoremap <Space> :w<CR>
" - split resizing
if bufwinnr(1)
  map + <C-W>+
  map _ <C-W>-
  map <c-n> <c-w>>
  map <c-m> <c-w><
endif
" - equal to auto indent whole file
nnoremap == gg=G''
" - auto indent for html in php file
nnoremap =h= :se ft=html<CR>gg=G'':se ft=php<CR>
" - paste/yank from/to clipboard
map <C-c> "+
""""

" Autoreload vimrc
augroup reload_vimrc " {
  autocmd!
  autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END " }
""""

" Autocmds
" - remove trailing whitespace when type :w on normal mode
autocmd BufWritePre *.{h,c,hpp,cpp,java,py,html,css,js,php} :%s/\s\+$//e

" - auto-arrange the whole coding indentation field after reading the file into the buffer
autocmd BufReadPost *.{h,c,hpp,cpp,java} :normal gg=G
""""

" JsBeautify Shortcut
map <c-f> :call JsBeautify()<cr>
""""

" Vundle Settings
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" Common
Plugin 'scrooloose/nerdtree'
Plugin 'jiangmiao/auto-pairs'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'Yggdroot/indentLine'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-sensible'
Plugin 'vim-scripts/Conque-GDB'
Plugin 'vim-scripts/Conque-Shell'
"Plugin 'Valloric/YouCompleteMe'

" For language enhancement
"" Html
Plugin 'mattn/emmet-vim'
"" Pug
Plugin 'digitaltoad/vim-pug'
"" Swig
Plugin 'vim-scripts/swig'
"" Javascript
Plugin 'pangloss/vim-javascript'
Plugin 'maksimr/vim-jsbeautify'
"" Python
Plugin 'Vimjas/vim-python-pep8-indent'
Plugin 'hdima/python-syntax'
"" JuliaLang
Plugin 'JuliaEditorSupport/julia-vim'

call vundle#end()
filetype plugin on
""""

" Nerdtree Settings
nnoremap <F9> :NERDTreeToggle<CR>
" - Open NERDTree automatically when vim starts up on opening a directory
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
" - Close vim if the only window left open is a NERDTree?
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
""""

" Aireline Themes
let g:airline_theme='simple'
let g:airline_powerline_fonts = 1
""""

" Emmet settings
let g:user_emmet_leader_key='<TAB>'
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall
""""

" Conque-GDB settings
let g:ConqueTerm_Color = 2         " 1: strip color after 200 lines, 2: always with color
let g:ConqueTerm_CloseOnEnd = 1    " close conque when program ends running
let g:ConqueTerm_StartMessages = 0 " display warning messages if conqueTerm is configured incorrectly  
""""

" You Compelete Me settings
let g:ycm_server_python_interpreter='/usr/bin/python'
let g:ycm_global_ycm_extra_conf='~/.ycm_extra_conf.py'
let g:ycm_autoclose_preview_window_after_completion=1
""""

" ACM
map <C-A> ggVG"+y  
map <F6> :call Run()<CR>  
func! Run()  
  exec "w"  
  exec "!g++ -Wall %"  
  exec "!./a.out"
endfunc 
map <F5> :call RunOJ()<CR>  
func! RunOJ()  
  exec "w"  
  exec "!g++ -Wall %" 
  exec "!./a.out < input > output"
endfunc 
""""
